# demo allocation (front-end)

# Repo And Demo

This is a demo about a simple single page application.

 BACKEND (Symfony)

- [Backend Repository (GitLab)](https://gitlab.com/sergiosicari/allocation)

- [Backend Demo (Nelmio API Doc on Heroku)](https://desolate-dusk-20743.herokuapp.com/api/doc)

FRONTEND (Vue.js)

- [Frontend Repo (GitLab)](https://gitlab.com/sergiosicari/allocation-frontend)

- [Frontend Demo (GitLab Pages)](https://sergiosicari.gitlab.io/allocation-frontend)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

N.B.: every member allocation page are visible using member id in path. For example https://sergiosicari.gitlab.io/allocation-frontend/#/resource/1
