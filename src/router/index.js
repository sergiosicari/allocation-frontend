import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: require('@/components/Home').default,
      props: { msg: 'Welcome to Allocation SPA'}
    },
    {
      path: '/resource/:resource_id?',
      name: 'resource',
      component: require('@/components/Resources').default,
    },
    {
        path: '/management',
        name: 'management',
        component: require('@/components/Management').default,
    },
    {
        path: '*',
        redirect: '/',
    },
  ],
});